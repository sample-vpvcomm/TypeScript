/**
 * Created by p.vasin on 24.08.17.
 * точка сборки всех кдассов для использования
 */
import GtmCommon from './gtm-common';
import GtmViewport from './gtm-viewport';
import GtmHandler from './gtm-handler';
import GtmWidget from './gtm-widget';
import GtmSlider from './gtm-slider';
import GtmTrigger from './gtm-trigger';

let common: GtmCommon = new GtmCommon;
let viewport: GtmViewport = new GtmViewport;
let handler: GtmHandler = new GtmHandler;
let widget: GtmWidget = new GtmWidget;
let slider: GtmSlider = new GtmSlider;
let trigger: GtmTrigger = new GtmTrigger;

// глобальный идентификатор сайта
(<any>window).gtmSiteCurrent = common.siteIdentify;

// глобальная переменная для trigger-файлов
(<any>window).customEvents = common.customEvents;

// шаг номер = step_first =
jQuery(window).on('load', function () {
    let legend = jQuery('#delivery-fieldset .legend');
    let selector = legend.find('.active').attr('id');
    if (typeof selector !== 'undefined') {
        common.generateStepData(1,selector);
    }
});

jQuery(document).on('ready', function () {

    // --- 0. reconfig ---------------------------------------------------------------------------------

    common.customEvents.on('config-tag-man', function(evt,data) {
        if (data.hasOwnProperty('addConfig')) {
            jQuery.extend(
                true,
                GtmCommon.configTagMan,
                data.addConfig
            );
        }
    });

    common.customEvents.on('slider-config-tag-man', function(evt,data) {
        if (data.hasOwnProperty('revolution')) {
            common.selectorSliderRevolution = data.revolution;
        }
        if (data.hasOwnProperty('bx')) {
            common.selectorSliderBX = data.bx;
        }
    });

    // --- 1. common init ---------------------------------------------------------------------------------

    common.run();

    // 2. --- handler init ---------------------------------------------------------------------------------

    handler.run(common);

    // 3. --- viewport init ---------------------------------------------------------------------------------

    viewport.run(widget,slider);

    // 4. --- trigger init ---------------------------------------------------------------------------------

    trigger.run();

});