/**
 * Created by p.vasin on 24.08.17.
 * глобальные переменные для этапа компиляции
 * могут находиться в html-коде страницы
 */
declare let dataLayer: any;
declare let jsonPage: any;
declare let jsonBanners: any;
declare let jsonProducts: any;
declare let jsonSellProducts: any;
declare let jQuery: any;
declare let Ya: any;
declare let addthis: any;