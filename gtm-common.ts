/**
 * Created by p.vasin on 24.08.17.
 * общие для всех классов параметры
 */
export default class GtmCommon {

    /**
     * Список всех сайтов компании Aristos
     * @type {[string,string,string,string,string,string,string,string,string,string,string,string,string,string,string,string,string,string,string]}
     */
    public sitesAll: string[] = [
        "philips","pfse","olympus","castrol","castrol-original","grohe","grundfos",
        "tefal","rowetna","moulinex","krups","zte","myzte","imtoy",
        "beastmode","purina","philipsaudio","onkyo","pen",
    ];

    /**
     * Соответствие нестандартных названий сайтов оригинальному значению
     * @type {{pfse: string; castrol-original: string; myzte: string}}
     */
    public sitesMapping: any = {
        "pfse":"philips",
        "castrol-original":"castrol",
        "myzte":"zte"
    };

    /**
     * Определение сайта по его домену
     * необходимо для индивидуального конфигурирования
     * @type {string}
     */
    public siteIdentify = (function(context: any):string {
        let url = window.location.hostname;
        if (window.location.search.indexOf('?gtmSiteCurrent=') !== -1) {
            url = window.location.search.replace(/\?gtmSiteCurrent=/g,'');
        }
        let sitesAllRegString: string = context.sitesAll.join('|');
        let regex: RegExp = new RegExp(sitesAllRegString, "g");
        let mathchArray = url.match(regex);
        let result: string = mathchArray[0].toString();
        let checkMapping = context.sitesMapping[result];
        let currentSite = (typeof checkMapping !== 'undefined') ? checkMapping : result;
        return currentSite;
    })(this);

    /**
     * метод для выполнения главной логики
     * внутри jQuery document ready
     */
    public run(): void {

        let now = this;

        (<any>window).sliderRevolutionHome = jQuery(this.selectorSliderRevolution);

        (<any>window).bxSliderObject = jQuery(this.selectorSliderBX).bxSlider({
            auto: true,
            controls: false,
            onSliderLoad: function(index) {
                jQuery('body').trigger('bx-onSliderLoad',{index:index});
            },
            onSlideAfter: function(slideElement, oldIndex, newIndex) {
                jQuery('body').trigger('bx-onSlideAfter',{slideElement:slideElement,oldIndex:oldIndex,newIndex:newIndex});
            }
        });

        jQuery('#shipping-select-delivery').click(function(){
            now.generateStepData(1,'shipping-select-delivery');
        });
        jQuery('#shipping-select-pickup').click(function(){
            now.generateStepData(1,'shipping-select-pickup');
        });

        // шаг номер = step_second
        jQuery('#shipping-methods-row').click(function(){
            let item = jQuery(this).children('ul').find('.active').data('id');
            now.generateStepData(2,'courier',item);
        });
        jQuery('.pickup-methods').click(function(){
            let item = jQuery(this).find('.active').data('id');
            now.generateStepData(2,'pickup',item);
        });
        jQuery('#pickup_select').click(function(){
            let item = jQuery(this).attr('id');
            now.generateStepData(2,'pickup',item);
        });

        // шаг номер = step_third
        jQuery('#payment-methods').click(function(){
            let selector = jQuery(this).children('li.active').data('id');
            now.generateStepData(3,selector);
        });

        (<any>window).generateTriggerBannerClick = function (banner_id: string | number): void {
            this.customEvents.trigger('banner_click',{banner_id: banner_id});
        }
        
    }

    /**
     * объект для пользовательских событий
     * @type {{on: ((on:string, args:any)=>any); trigger: ((trigger:string, args:any)=>any)}}
     */
    public customEvents = (function(): any {
        let eventNode = jQuery({});
        return {
            on: on,
            trigger: trigger
        };
        function on(){
            eventNode.on.apply(eventNode, arguments);
        }
        function trigger(){
            eventNode.trigger.apply(eventNode, arguments);
        }
    })();

    /**
     * селекторы для слайдеров по-умолчанию
     * @type {string}
     */
    public selectorSliderRevolution = '' +
        '.slider-banner-container .slider-philips,' +
        '.main-slider-wrap .tp-banner,' +
        '.fullwidthbanner-container .fullwidthbanner-olympus';
    public selectorSliderBX = '' +
        '.mb_slider_container,' +
        '.mb_container';

    /**
     * генерация данных для шага в checkout
     * @param step
     * @param selector
     * @param item
     */
    public generateStepData(step: string | number, selector: string, item?: string): void {
        let nodeStep = GtmCommon.gtmCheckoutStepsConfig[step];
        let trigger = nodeStep.trigger.step;
        let option = nodeStep.selector[selector];
        if (item) {
            option = nodeStep.selector[selector][item];
        }
        this.customEvents.trigger(trigger,{
            step: step,
            option: option
        });
    }

    /**
     * получить значение конфига по ключу
     * @param string key - parent/sku/shown/pill
     * @returns {any}
     */
    public static getProdConf(key: string): string | boolean {
        return this.configTagMan.selector.widget.products[key];
    }

    /**
     * конфигурация
     * @type object
     */
    public static configTagMan = {
        "selector":{
            "widget":{
                "products":{
                    "parent":".products-grid",
                    "sku":".sku,.product-sku",
                    "shown":".products-grid li,.listing-item",
                    "pill":false
                }
            }
        }
    };


    /**
     * конфиг учета шагов пользователя в checkout
     * @type object
     */
    public static gtmCheckoutStepsConfig = {
        "1":{
            "trigger":{
                "step":"step_first"
            },
            "selector":{
                "shipping-select-delivery":"courier",
                "shipping-select-pickup":"pickup"
            }
        },
        "2":{
            "trigger":{
                "step":"step_second"
            },
            "selector":{
                "courier":{
                    "aristos_du":"office",
                    "citycourier_base":"our_courier",
                    "deliveryems_deliveryems":"transport_company"
                },
                "pickup":{
                    "1":"office",
                    "pickup_select":"pickup_point"
                }
            }
        },
        "3":{
            "trigger":{
                "step":"step_third"
            },
            "selector":{
                "cloudpayments":"card_online",
                "cashondelivery":"cash",
                "cardoffline":"card_courier",
                "sbrf":"bank_receipt"
            }
        }
    };

    public test = 'class GtmCommon';

    public toConsole(): void {
        console.log(this.test);
    }    

}