/**
 * Created by p.vasin on 24.08.17.
 * обработчик событий
 */
import GtmCommon from './gtm-common';
import GtmObject from './gtm-object';
export default class GtmHandler {

    /**
     * получение данных по шагу
     * @param data
     */
    public addStepInfo(data: any): void {
        let products = dataLayer[0].ecommerce.checkout.products;
        let infoStep = GtmObject.getInfoStep(data,products);
        dataLayer.push(infoStep);
    }

    /**
     * выполнение логики на document ready
     * @param GtmCommon commonObject
     */
    public run(commonObject: GtmCommon): void {

        let common = commonObject;
        let now = this;

        // удаление SeenProducts там где нет блока
        let seenBlockAtPage = jQuery('.block.viewed, #accordion-viewed, .block-viewed');
        if (seenBlockAtPage.length<0) {
            let arrMiddle = [];
            jQuery.map(dataLayer["0"].ecommerce.impressions, function(elem, index){
                if (elem.list !== "SeenProducts") {
                    arrMiddle.push(elem);
                }
            });
            dataLayer["0"].ecommerce.impressions = arrMiddle;
        }

        // --- Banner Click Anywhere ------------

        // клик по баннеру = banner_click = +++
        common.customEvents.on('banner_click', function(evt,data) {
            let bann_id = data.banner_id;
            let bannerClick = GtmObject.getBannerClickObject();
            let bannerClickEcommerce = GtmObject.getBannerClickEcommerceObject();
            let targetBanner = null;
            if (typeof bann_id === 'number' && jsonBanners[bann_id] !== 'undefined') {
                targetBanner = jsonBanners[bann_id];
            } else if (typeof bann_id === 'string') {
                for (let key in jsonBanners) {
                    if (bann_id === jsonBanners[key]['src']) {
                        targetBanner = jsonBanners[key];
                        break;
                    }
                }
            }
            if (targetBanner) {
                bannerClickEcommerce.id = targetBanner.id;
                bannerClickEcommerce.name = targetBanner.name;
                bannerClickEcommerce.creative = targetBanner.creative;
                bannerClickEcommerce.position = targetBanner.position;
            }
            bannerClick.ecommerce.promoClick.promotions = bannerClickEcommerce;
            dataLayer.push(bannerClick);

        });

        // --- CategoryGroup ----------------------------------------------------------------------------------------------

        if (jsonPage.pageCategory === 'CategoryGroup') {

            // добавить товар в сравнение = product_add_compare = +++
            common.customEvents.on('product_add_compare', function(evt,data) {
                let prod_id = data.prod_id;
                let is_checked = data.is_checked;
                let fullJsonProducts = function ()
                {
                    let cloneImpressionsAll = jQuery.extend(true,{},dataLayer[0].ecommerce.impressions);
                    let middleStore = {};
                    for (let key in cloneImpressionsAll) {
                        let keyInside = cloneImpressionsAll[key]['id'];
                        middleStore[keyInside] = cloneImpressionsAll[key];
                    }
                    let addJsonProducts = (typeof (<any>window).jsonProducts !== 'undefined') ? {} : (<any>window).jsonProducts;
                    return jQuery.extend(true,middleStore,addJsonProducts);
                };
                let compareObject = jQuery.extend(true,{},jsonPage.productAddCompare);
                let product = fullJsonProducts[prod_id];
                if (typeof product !== 'undefined') {
                    delete compareObject.ecommerce;
                    if (is_checked) {
                        compareObject.eventAction = 'add';
                    } else {
                        compareObject.eventAction = 'remove';
                    }
                    compareObject.eventProductId = prod_id;
                    compareObject.eventPosition = fullJsonProducts[prod_id].position;
                    compareObject.eventProductPrice = fullJsonProducts[prod_id].price;
                    dataLayer.push(compareObject);
                }
            });

            // сортировка товаров = product_sort = +++
            common.customEvents.on('product_sort', function(evt,data) {
                let productSort = GtmObject.getProductSort();
                let eventContent = data.eventContent;
                productSort.eventContent = eventContent;
                dataLayer.push(productSort);
            });
        }

        // --- Checkout -----------------------------------------------------

        if (jsonPage.pageCategory === 'Checkout') {

            // первый шаг в checkout = step_first = +++
            common.customEvents.on('step_first', function(evt,data) {
                now.addStepInfo(data);
            });

            // второй шаг в checkout = step_second = +++
            common.customEvents.on('step_second', function(evt,data) {
                now.addStepInfo(data);
            });

            // третий шаг в checkout = step_third = +++
            common.customEvents.on('step_third', function(evt,data) {
                now.addStepInfo(data);
            });

            // удаление товара из корзины = product_remove_basket = +++
            common.customEvents.on('product_remove_basket', function(evt,data) {
                jsonPage.allBasket.ecommerce.remove.actionField.list = jsonPage.pageCategory;
                let prod_id = data.prod_id;
                let position = 1;
                jQuery.each(jsonPage.allBasket.ecommerce.remove.actionField.products, function(key, val){
                    if (typeof val  === 'object') {
                        if (val.id !== prod_id) {
                            delete jsonPage.allBasket.ecommerce.remove.actionField.products[key];
                        } else {
                            jsonPage.allBasket.ecommerce.remove.actionField.products[key].list = jsonPage.pageCategory;
                            jsonPage.allBasket.ecommerce.remove.actionField.products[key].dimension18 = '1';
                            jsonPage.allBasket.ecommerce.remove.actionField.products[key].position = position;
                        }
                        position++;
                    }
                });
                dataLayer.push(jsonPage.allBasket);
            });

            // очистка корзины = basket_clear = +++
            common.customEvents.on('basket_clear', function(evt,data) {
                jQuery.each(jsonPage.allBasket.ecommerce.remove.actionField.products, function(key, val){
                    jsonPage.allBasket.ecommerce.remove.actionField.products[key].list = jsonPage.pageCategory;
                });
                dataLayer.push(jsonPage.allBasket);
            });

            // клик по кнопке "Заказать" = order_confirm = +++
            common.customEvents.on('order_confirm', function(evt,data) {
                let orderConfirm = GtmObject.getOrderConfirm();
                dataLayer.push(orderConfirm);
            });

            // выбор способа доставки = shipping_select = +++
            common.customEvents.on('shipping_select', function(evt,data) {
                let shipMethod = data.shipMethod;
                let shippingSelect = GtmObject.getShippingSelect(shipMethod);
                dataLayer.push(shippingSelect);
            });

            // клик по кнопке "Пересчитать/Обновить" заказ = order_update = +++
            common.customEvents.on('order_update', function(evt,data) {

                // обновление данных корзины в dataLayer из-за pjax
                let inputQty = jQuery('form#cart input:text[name*="cart"]').parents('form .row-fluid');

                let allBasket = jsonPage.allBasket.ecommerce.remove.actionField.products;
                let newDataLayer = dataLayer[0].ecommerce.checkout.products;
                let storeBasket = {};

                jQuery.each(inputQty,function(){
                    let item = jQuery(this);
                    let sku = item.find('.sku').text().replace(/\n/g, '').trim();
                    let value = item.find('input:text[name*="cart"]').val().replace(/\n/g, '').trim();
                    storeBasket[sku] = value;
                });

                for (let i=0; i<newDataLayer .length;i++) {
                    for (let key in newDataLayer[i]) {
                        let sku2 = newDataLayer[i]['id'];
                        newDataLayer[i]['quantity'] = storeBasket[sku2];
                        allBasket[i]['quantity'] = storeBasket[sku2];
                    }
                }

                let orderUpdate = GtmObject.getOrderUpdate();
                dataLayer.push(orderUpdate);
            });

        }

        // --- ProductPage --------------------------------------------------

        if (jsonPage.pageCategory == 'ProductPage') {

            let socialIcon = GtmObject.getSocialIcon();

            if (jQuery('#ya-share2').length) {
                Ya.share2('ya-share2', {
                    hooks: {
                        onshare: function (name) {
                            let share = Ya.share2('ya-share2');
                            let services = share._services;
                            let i,url,label;
                            for (i = 0; i < services.length; i++) {
                                if (services[i].name === name) {
                                    label = services[i].title;
                                    url = services[i].location;
                                }
                            }
                            socialIcon.eventLabel = label;
                            socialIcon.eventContent = url;
                            dataLayer.push(socialIcon);
                        }
                    }
                });
            }

            // переключение между табами = tab_switch = +++
            common.customEvents.on('tab_switch', function(evt,data) {
                let eventContent = data.eventContent;
                jsonPage.tabSwitch.eventContent = eventContent;
                dataLayer.push(jsonPage.tabSwitch);
            });
        }




        // --- All PAges -----------------------------------------------------

        // добавление товра в корзину = product_add_basket = +++
        common.customEvents.on('product_add_basket', function(evt,data) {
            let curPage = jsonPage.pageCategory;
            let currentProduct, productAddBasket;

            // получаем первые 18 товаров
            let prodsFromDL0 = {};
            // проверка для главной страницы
            if (typeof dataLayer[0].ecommerce !== "undefined") {
                jQuery.map(dataLayer[0].ecommerce.impressions,function(elem, index){
                    prodsFromDL0[elem.id] = elem;
                });
            }

            // ProductPage = CategoryGroup = SearchResults

            if (curPage === 'ProductPage') {
                productAddBasket = jsonPage.productAddBasket;
            } else {
                // все товары
                let fullJsonProds = jQuery.extend(prodsFromDL0,(<any>window).jsonProducts);
                let prod_id = data.prod_id;
                currentProduct = fullJsonProds[prod_id];
                if (typeof currentProduct !== 'undefined') {
                    productAddBasket = GtmObject.getProductAddBasket(currentProduct);
                    currentProduct.list = curPage;
                    currentProduct.quantity = 1;
                    currentProduct.coupon = "";
                    delete currentProduct.id_cat;
                    productAddBasket.ecommerce.add.actionField = currentProduct;
                }
            }
            dataLayer.push(productAddBasket);

        });


        // клик по иконке соцсетей = social_icon =
        common.customEvents.on('social_icon', function(evt,data) {
            let label = data.label;
            let url = data.url;
            let socialIcon = GtmObject.getSocialIcon();
            socialIcon.eventLabel = label;
            socialIcon.eventContent = url;
            dataLayer.push(socialIcon);
        });


        // авторизация пользователя = user_login = +++
        common.customEvents.on('user_login', function(evt,data) {
            let userLogin = GtmObject.getUserLogin();
            dataLayer.push(userLogin);
        });

        // клик по товару = product_click =
        common.customEvents.on('product_click', function(evt,data) {
            let prod_id = data.prod_id;
            let product;
            if (typeof (<any>window).jsonProducts !== 'undefined' && typeof (<any>window).jsonProducts[prod_id] !== 'undefined') {
                product = (<any>window).jsonProducts[prod_id];
            } else {
                let impress = dataLayer[0].ecommerce.impressions;
                jQuery.map(impress, function (elem, index) {
                    if (elem.id === prod_id) {
                        product = elem;
                    }
                });
            }
            let productClick = GtmObject.getProductClick(product, jsonPage);
            dataLayer.push(productClick);
        });
    }

    public test = 'class GtmHandler';

    public toConsole(): void {
        console.log(this.test);
    }

}