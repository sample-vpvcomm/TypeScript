/**
 * Created by p.vasin on 24.08.17.
 * тела объектов для пуша в dataLayer
 */
export default class GtmObject {

    /**
     * шаблон объекта для пуша в dataLayer
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string}}
     */
    public static getOrderUpdate(): any {
        let orderUpdate = {
            "event": "aristos",
            "eventCategory": "Interactions",
            "eventAction": "click",
            "eventLabel": "recountOrder"
        };
        return orderUpdate;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @param product
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, ecommerce: {click: {actionField: {list: string}, products: {name, id, price, brand: any, category, variant: any, position}[]}}}}
     */
    public static getProductClick(product: any, jsonPage: any): any {
        let productClick = {
            "event":"aristos",
            "eventCategory": "Interactions",
            "eventAction": "click",
            "eventLabel": "product",
            "ecommerce": {
                "click": {
                    "actionField": {"list": jsonPage.pageCategory ? jsonPage.pageCategory : "Unknown"}, // Optional list property.
                    'products': [{
                        "name": product.name,
                        "id": product.id,
                        "price": product.price,
                        "brand": product.brand,
                        "category": product.category,
                        "variant": product.variant,
                        "position": product.position
                    }]
                }
            }
        };
        return productClick;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string}}
     */
    public static getUserLogin() {
        let userLogin = {
            "event": "aristos",
            "eventCategory": "Interactions",
            "eventAction": "click",
            "eventLabel": "signIn"
        };
        return userLogin;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @param currentProduct
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, eventLocation, eventPosition, eventCategoryId: any, eventProductId, eventProductPrice, ecommerce: {add: {actionField: {}}}}}
     */
    public static getProductAddBasket(currentProduct: any): any {
        let productAddBasket = {
            "event": "aristos",
            "eventCategory": "Conversions",
            "eventAction": "add",
            "eventLabel": "cart",
            "eventLocation": currentProduct.category, // название блока с товарами
            "eventPosition": currentProduct.position, // порядковый номер товара в блоке
            "eventCategoryId": currentProduct.id_cat, // id категории товара
            "eventProductId": currentProduct.id, // id товара
            "eventProductPrice": currentProduct.price, // цена товара
            "ecommerce": {
                "add": {
                    "actionField": {}
                }
            }
        };
        return productAddBasket;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, eventContent: string}}
     */
    public static getSocialIcon(): any {
        let socialIcon = {
            "event": "aristos",
            "eventCategory": "Social",
            "eventAction": "click",
            "eventLabel": "",
            "eventContent": ""
        };
        return socialIcon;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @param shipMethod
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, ecommerce: {checkout_option: {actionField: {option: any}}}}}
     */
    public static getShippingSelect(shipMethod: any): any {
        var shippingSelect = {
            "event": "aristos",
            "eventCategory": "Interactions",
            "eventAction": "switch",
            "eventLabel": "deliveryMethod",
            "ecommerce": {
                "checkout_option": {
                    "actionField": {
                        "option": shipMethod
                    }
                }
            }
        };
        return shippingSelect;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, eventContent: string}}
     */
    public static getOrderConfirm(): any {
        let orderConfirm = {
            "event": "aristos",
            "eventCategory": "Conversions",
            "eventAction": "click",
            "eventLabel": "confirmOrder",
            "eventContent": "successful"
        };
        return orderConfirm;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @param data
     * @param products
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel, ecommerce: {checkout: {actionField: {step, option}, products: any}}}}
     */
    public static getInfoStep(data: any, products: any): any {
        let infoStep = {
            "event":"aristos",
            "eventCategory": "Interactions",
            "eventAction": "checkout step",
            "eventLabel": data.step,
            'ecommerce': {
                "checkout": {
                    "actionField": {"step": data.step, "option": data.option},
                    "products": products
                }
            }
        };
        return infoStep;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, eventContent: string}}
     */
    public static getProductSort(): any {
        let productSort = {
            "event": "aristos",
            "eventCategory": "Interactions",
            "eventAction": "click",
            "eventLabel": "sort",
            "eventContent": ""
        };
        return productSort;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @returns {{id: string, name: string, creative: string, position: string}}
     */
    public static getBannerClickEcommerceObject(): any {
        let bannerClickEcommerce = {
            "id": "",
            "name": "",
            "creative": "",
            "position": ""
        };
        return bannerClickEcommerce;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, ecommerce: {promoClick: {promotions: {}}}}}
     */
    public static getBannerClickObject(): any {
        let bannerClick = {
            "event": "aristos",
            "eventCategory": "Interactions",
            "eventAction": "click",
            "eventLabel": "banner",
            "ecommerce": {
                "promoClick": {
                    "promotions": {}
                }
            }
        };
        return bannerClick;
    }

    /**
     * шаблон объекта для пуша в dataLayer
     * @param impressions
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, ecommerce: {impressions: any}}}
     */
    public static getEventObject(impressions: any): any {
        let result = {
            "event":"aristos",
            "eventCategory": "Non-Interactions",
            "eventAction": "show",
            "eventLabel": "products",
            "ecommerce":{
                "impressions": impressions
            }
        };
        return result;
    }

    /**
     * шаблон объекта просмотренных баннеров для пуша
     * @returns {{event: string, eventCategory: string, eventAction: string, eventLabel: string, ecommerce: {promoView: {promotions: Array}}}}
     */
    public static getPromoViewObject(): any {
        let promo = {
            "event":"aristos",
            "eventCategory": "Non-Interactions",
            "eventAction": "show",
            "eventLabel": "banners",
            "ecommerce":{
                "promoView": {
                    "promotions": []
                }
            }
        };
        return promo;
    }

    /**
     * шаблон promotion-объекта
     * @returns {{id: string, name: string, creative: string, position: string}}
     */
    public static getPromotionObject(): any {
        let promo = {
            "id": "",
            "name": "",
            "creative": "",
            "position": ""
        };
        return promo;
    }

}