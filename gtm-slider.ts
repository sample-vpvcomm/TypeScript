/**
 * Created by p.vasin on 24.08.17.
 * работа со слайдерами
 */
import GtmObject from './gtm-object';
import GtmViewportParent from './gtm-viewport-parent';
export default class GtmSlider extends GtmViewportParent {

    /**
     * добавление баннера в dataLayer
     * @param bannerObj
     */
    public gtmPushOneBanner(bannerObj: any): void {
        let promoObject = GtmObject.getPromoViewObject();
        if (this.gtmFocusedBrowser) {
            delete bannerObj.src;
            promoObject.ecommerce.promoView.promotions.push(bannerObj);
            dataLayer.push(promoObject);
        }
    }

    /**
     * проверка позиции баннера
     * @param bannerSet
     */
    public gtmCheckBannerPosition(bannerSet: any): void {

        let thisClass = this; // для использования внутри let
        let middleStore: any = {};
        clearTimeout(this.gtmPushTimerBanners);

        this.gtmPushTimerBanners = setTimeout(function () {

            let clone: any = GtmObject.getPromoViewObject();

            jQuery.each(bannerSet, function () {
                let item = jQuery(this);
                let bannerID = item.data('banner-id');
                let checkViewport = thisClass.gtmCheckElementInViewport(item);
                if (checkViewport && jsonBanners[bannerID] !== 'undefined') {
                    let bannerForPush = jsonBanners[bannerID];
                    middleStore[bannerID] = bannerForPush;
                }
            });

            jQuery.map(middleStore, function (elem) {
                delete elem["src"];
                clone.ecommerce.promoView.promotions.push(elem);
            });

            // пушим только если есть что пушить
            if (clone.ecommerce.promoView.promotions.length > 0) {
                dataLayer.push(clone);
            }

        }, this.gtmPushTimerCycle);
    }

    public bxSliderRecord (): void {

        let now = this;

        let slider = (<any>window).bxSliderObject;
        let selector = slider.selector;

        // console.log('VIEWPORT = window.bxSliderObject',window.bxSliderObject);

        let bxImages = jQuery(selector + " img:visible[data-banner-id]").length > 0
            ? jQuery(selector + " img:visible[data-banner-id]")
            : jQuery(selector + " img:visible[data-banner]");

        // учет просмотра баннера после клика
        jQuery('.bx-pager-item').on('click', function () {
            let current = slider.getCurrentSlide();
            slider.goToSlide(current);
        });

        if (typeof bxImages !== 'undefined' && bxImages.length > 0) {

            jQuery('body').on('bx-onSliderLoad', function(evt,data) {

                clearTimeout(now.gtmBxPushTimer);
                now.gtmBxPushTimer = setTimeout(function(e){

                    let bannerIndex = data.index;
                    let showImgSlide = now.gtmCheckElementInViewport(bxImages[bannerIndex]);
                    if (showImgSlide) {
                        let banner = jQuery(bxImages[bannerIndex]);
                        let bannerID = banner.data('banner-id') ? banner.data('banner-id') : banner.data('banner');
                        let bannerObj = jsonBanners[bannerID];
                        let objectLength = Object.keys(bannerObj).length;
                        if (objectLength > 0) {
                            bannerObj.creative = now.gtmCreativeForSlider;
                            now.gtmPushOneBanner(bannerObj);
                        }
                    }

                }, now.gtmPushTimerCycle);

            });

            jQuery('body').on('bx-onSlideAfter', function(evt,data) {

                clearTimeout(now.gtmBxPushTimer);
                now.gtmBxPushTimer = setTimeout(function(e){

                    let slideElement = jQuery(data.slideElement);
                    let showImgSlide = now.gtmCheckElementInViewport(data.slideElement);
                    if (showImgSlide) {
                        let imgSlide = slideElement.find('img'),
                            imgSlideID = imgSlide.data('banner-id')
                                ? imgSlide.data('banner-id')
                                : imgSlide.data('banner');
                        let bannerObj = jsonBanners[imgSlideID];
                        let objectLength = Object.keys(bannerObj).length;
                        if (objectLength > 0) {
                            bannerObj.creative = now.gtmCreativeForSlider;
                            now.gtmPushOneBanner(bannerObj);
                        }
                    }

                }, this.gtmPushTimerCycle);

            });
        }

    }

    public test = 'class GtmSlider';

    public toConsole(): void {
        console.log(this.test);
    }

}