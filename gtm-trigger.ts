/**
 * Created by p.vasin on 29.08.17.
 * общий класс для сбора событий
 * на всех сайтах
 */
import GtmObject from './gtm-object';
import GtmCommon from './gtm-common';
export default class GtmTrigger {

    public selector: any = {
        "order_confirm":'#submit-order',
        "shipping_select":'*[data-toggle*="shipping-select"]',
        "order_update":'button[value="update_qty"]',
        "banner_slider_click":'.tp-caption',
        "banner_click":'img[data-banner-id]',
        "product_click":'.products-grid .item a,' +
                        '.listing-item a,' +
                        '.products-grid .item-content a,' +
                        '.masonry-grid-item a',
        "product_add_compare":'input:checkbox[name="compare"]',
        "product_add_basket":'.btn-in-stock,' +
                                '.button-in-cart,' +
                                '.btn-preorder,' +
                                '.btn-cart,' +
                                '.button-in-stock',
        "product_sort":'.product-sort-by li a,' +
                        '.sort-by li a,' +
                        '.sort-by option,' +
                        '#sort-select,' +
                        '#sort-select-menu',
        "product_remove_basket":'#shopping-cart .remove,' +
                                'form#cart .remove,' +
                                '.btn-remove,' +
                                '.remove .btn',
        "basket_clear":'button[value="empty_cart"]',
        "tab_switch":'ul[class*="nav-tabs"] li,' +
                        '#prod_tabs ul.nav li,' +
                        '#accordion h3 span,' +
                        '.nav-tabs a',
        "social_icon":'.ya-share2__item,' +
                        '.at-share-btn,' +
                        '.social-share li a',
        "user_login":'#btn-register,#btnRecoveryPass, .btn-orange, div[class*="ulogin-"], ' +
                        'a[href*="account/create"], a[href*="customer/account"], .login-form button,' +
                        'a[href*="customer/account/create"], a[href*="customer/account/login"], ' +
                        '#account-popover-wrap .popover-btn, a[href*="account/login"]'
    };

    /**
     * Клик по кнопке "Заказать"
     * order_confirm
     * @param context
     */
    public orderConfirm(context: any): void {
        this.common.customEvents.trigger('order_confirm');
    }

    /**
     * Выбор способа доставки: курьером/самостоятельно
     * shipping_select
     * wtf to deploy
     * @param context
     */
    public shippingSelect(context: any): void {
        let shipMethod = jQuery(context).text().replace(/\n/g, '').trim();
        this.common.customEvents.trigger('shipping_select',{shipMethod: shipMethod});
    }

    /**
     * Пересчитать/Обновить заказ
     * order_update
     * @param context
     */
    public orderUpdate(context: any): void {
        this.common.customEvents.trigger('order_update');
    }

    public common: GtmCommon;

    constructor() {
        this.common = new GtmCommon;
    }

    private generateTriggerBannerClick(bannerId: string | number): void{
        this.common.customEvents.trigger('banner_click',{banner_id: bannerId});
    }

    /**
     * Получение SKU-товара
     * унифицированный метод
     * @param context jQuery(this)
     * @returns {string}
     */
    private getSku =(context: any): string => {
        let parents: string = '.masonry-grid-item, .listing-item, #product-description, #product-info, .product-shop, .item, li';
        let prodId: string = jQuery(context).parents(parents).find('.sku').text().replace(/\n/g, '').trim();
        let prodIdData1: string = jQuery(context).parents('li').data('sku');
        let prodIdData2: string = jQuery(context).parents('.product-shop').data('sku');
        let prodIdData: string = (prodId + prodIdData1 + prodIdData2).replace(/undefined/g,'').trim();
        if (prodIdData !== '') {
            prodId = prodIdData.replace(/Артикул |Sku: |Артикул: |Код товара: |Арт./g, '').trim();
        }
        return prodId;
    };

    /**
     * ! требуется уточнение по информации о баннере
     * Клик по баннеру в слайдере
     * banner_click
     * @param context jQuery(this)
     */
    public bannerSliderClick(context: any): void {
        let bannerId = jQuery(context).parents('li').data('link');
        if (bannerId[0] === '/') {
            bannerId = bannerId.substr(1);
        }
        this.generateTriggerBannerClick(bannerId);
    }

    /**
     * Клик по статическому баннеру с data-banner-id
     * banner_click
     * @param context jQuery(this)
     */
    public bannerClick(context: any): void {
        let bannerId = jQuery(context).data('banner-id');
        this.generateTriggerBannerClick(bannerId);
    }

    /**
     * Клик по товару в листинге: ссылка текст/картинка
     * product_click
     * @param context jQuery(this)
     */
    public productClick(context: any): void {
        let prodId: string = this.getSku(context);
        let prodUrl: string = jQuery(context).attr('href');
        this.common.customEvents.trigger('product_click',{prodId: prodId, prod_url: prodUrl});
    }

    /**
     * Добавление товара в сравнение
     * product_add_compare
     * @param context jQuery(this)
     */
    public productAddCompare(context: any): void {
        let prodId = this.getSku(context);
        let isChecked = jQuery(context).prop('checked');
        this.common.customEvents.trigger('product_add_compare',{prodId: prodId,is_checked:isChecked});
    }

    /**
     * Добавление товара в корзину
     * product_add_basket
     * @param context jQuery(this)
     */
    public productAddBasket(context: any): void {
        this.common.customEvents.trigger('product_add_basket',{prodId:this.getSku(context)});
    }

    /**
     * Сортировка товаров на странице листинга
     * product_sort
     * @param context jQuery(this)
     */
    public productSort(context: any): void {
        let eventType: string|any = event.type;
        let eventTarget: string|any = event.target;
        let content: string;
        switch(eventType) {
            case 'click':
                content = jQuery(context).text();
                break;
            case 'change':
                content = jQuery(context).find('option:selected').text();
                break;
        }
        let eventContent = content.replace(/\n/g,'').trim();
        this.common.customEvents.trigger('product_sort',{eventContent: eventContent});
    }

    /**
     * Получение данных при удалении одного товара из корзины
     * product_remove_basket
     * @param context
     */
    public productRemoveBasket(context: any): void {
        let prodIdAll: string[] = [
            jQuery(context).parents('.tag-man').data('sku'),
            jQuery(context).prevAll('.product').find('.sku').text(),
            jQuery(context).parents('.row-fluid').find('.product small').text().replace(/\n/g,'').replace('* Sku: ','').trim(),
            jQuery(context).parents('.row-fluid').find('.prod-name small').text().replace(/\n/g,'').replace('* Sku: ','').trim(),
            jQuery(context).parents('tr').find('.item-msg:contains("Sku")').text().replace(/\n/g,'').replace('* Sku: ','').trim()
        ];
        let prodIdAllString = prodIdAll.join('');
        let prodId: string = prodIdAllString.replace(/undefined/g,'').trim();
        this.common.customEvents.trigger('product_remove_basket',{prodId: prodId});
    }

    /**
     * Удаление всех товаров из корзины
     * basket_clear
     * @param context
     */
    public basketClear(context: any): void {
        this.common.customEvents.trigger('basket_clear');
    }

    /**
     * Переключение между табами на карточке товаров
     * tab_switch
     * @param context jQuery(this)
     */
    public tabSwitch(context: any): void {
        let eventContent = jQuery(context, 'a').text().replace(/\n/g, '').trim();
        this.common.customEvents.trigger('tab_switch',{eventContent: eventContent});
    }

    /**
     * Извлечение домена из URL
     * @param url
     * @returns {string}
     */
    public extractDomain(url: string): string {
        let regex = /(?:[\w-]+\.)+[\w-]+/;
        let domain: string = (typeof url !== "undefined") ? regex.exec(url).toString() : "unknow-url";
        return domain;
    }

    /**
     * Объект для получения label соцсети
     * по домену
     * @type {{[vk.com]: string; [www.facebook.com]: string; [connect.ok.ru]: string; [connect.mail.ru]: string; [plus.google.com]: string}}
     */
    public socialList: any = {
        "vk.com": "ВКонтакте",
        "www.facebook.com": "Facebook",
        "connect.ok.ru": "Одноклассники",
        "connect.mail.ru": "Мой Мир",
        "plus.google.com": "Google+"
    };

    public socialIcon(context: any, evt?: any): void {
        let url = jQuery(context).find('a').attr('href');
        let domain = this.extractDomain(url);
        let label: string = this.socialList[domain];
        this.common.customEvents.trigger('social_icon',{label: "label", url: "url"});
    }

    public userLogin(context: any): void {
        this.common.customEvents.trigger('user_login');
    }

    // ----------------------------------------

    public run(): void {

        let now = this;

        // click addthis share button
        function shareEventHandler(evt) {
            if (evt.type == 'addthis.menu.share') {
                let evtData = evt.data;
                (<any>window).customEvents.trigger('social_icon',{label: evtData.service, url: "addthis-unknown-url"});
            }
        }
        if (typeof addthis !== 'undefined') {
            addthis.addEventListener('addthis.menu.share', shareEventHandler);
        }

        /* --- BANNER CLICK ------------------------------------------------------------------------------------------ */

        // клик по баннеру-ссылке в слайдере Revolution
        jQuery(now.selector.banner_slider_click).click(function(event){
            now.bannerSliderClick(this);
        });

        // клик по баннеру-картинке = banner_click
        jQuery(now.selector.banner_click).click(function(event){
            now.bannerClick(this);
        });

        /* --- EVENTS -------------------------------------------------------------------------------------------------- */

        // клик по товару = product_click =
        jQuery(now.selector.product_click).click(function(){
            now.productClick(this);
        });

        // добавить товар в сравнение = product_add_compare =
        jQuery(now.selector.product_add_compare).click(function(){
            now.productAddCompare(this);
        });


        // сортировка товаров = product_sort =
        jQuery(now.selector.product_sort).on('change click', function(event) {
            now.productSort(this);
        });


        // удаление товара из корзины = product_remove_basket =
        jQuery(now.selector.product_remove_basket).click(function(){
            now.productRemoveBasket(this);
        });


        // очистка корзины = basket_clear =
        jQuery(now.selector.basket_clear).click(function(){
            now.basketClear(this);
        });


        // клик по кнопке "Заказать" = order_confirm =
        jQuery(now.selector.order_confirm).click(function(){
            now.orderConfirm(this);
        });


        // выбор способа доставки = shipping_select =
        jQuery(now.selector.shipping_select).click(function(){
            now.shippingSelect(this);
        });


        // клик по кнопке "Пересчитать/Обновить" заказ = order_update =
        jQuery(now.selector.order_update).click(function(){
            now.orderUpdate(this);
        });


        // переключение между табами = tab_switch =
        jQuery(now.selector.tab_switch).click(function(){
            now.tabSwitch(this);
        });

        // добавление товра в корзину = product_add_basket =
        jQuery(now.selector.product_add_basket).click(function(){
            now.productAddBasket(this);
        });

        // клик по иконке соцсетей = social_icon =
        jQuery(now.selector.social_icon).click(function(event){
            now.socialIcon(this);
        });

        // авторизация пользователя = user_login =
        jQuery(now.selector.user_login).click(function(){
            now.userLogin(this);
        });

    }

}