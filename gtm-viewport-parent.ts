/**
 * Created by p.vasin on 25.08.17.
 * родитель для классов участвующих во viewport
 */
export default class GtmViewportParent {

    // таймеры
    protected gtmPushTimer: any = null;
    protected gtmPushTimerProducts: any = null;
    protected gtmPushTimerBanners: any = null;
    protected gtmBxPushTimer: any = null;

    // таймер для цикличности пушинга
    protected gtmPushTimerCycle: string | number = 2000;

    // размер сегмента
    protected chunkSize: number = 18;

    // проверка фокусировки браузера
    public gtmFocusedBrowser: boolean = true;

    // обозначение баннеров из слайдеров
    public gtmCreativeForSlider: string = "slider";

    // накопитель просмотренных продуктов
    public gtmImpressProducts: any = {};

    /**
     * сбор данных об экране
     * @returns {{screenWidth: any, screenHeight: any, scrollTop: any, scrollLeft: any, seeX1: any, seeX2: any, seeY1: any, seeY2: any}}
     */
    public gtmAboutScreen(): any {

        // габариты экрана
        let screenWidth = jQuery(window).width();
        let screenHeight = jQuery(window).height();
        let scrollTop = jQuery(document).scrollTop();
        let scrollLeft = jQuery(document).scrollLeft();

        // координаты углов видимой области
        let seeX1 = scrollLeft;
        let seeX2 = screenWidth + scrollLeft;
        let seeY1 = scrollTop;
        let seeY2 = screenHeight + scrollTop;

        return {
            "screenWidth":screenWidth,
            "screenHeight":screenHeight,
            "scrollTop":scrollTop,
            "scrollLeft":scrollLeft,
            "seeX1":seeX1,
            "seeX2":seeX2,
            "seeY1":seeY1,
            "seeY2":seeY2
        };

    }

    /**
     * сбор данных об элементе
     * @param element - jQuery object
     * @returns {{positionElement: any, elemTop: any, elemLeft: any, elemWidth: any, elemHeight: any, elemX1: any, elemX2: any, elemY1: any, elemY2: any}}
     */
    public gtmAboutElement (element: any): any {

        // параметры элемента
        let positionElement = jQuery(element).offset();
        let elemTop = positionElement.top;
        let elemLeft = positionElement.left;
        let elemWidth = jQuery(element).width();
        let elemHeight = jQuery(element).height();

        // координаты элемента
        let elemX1 = elemLeft;
        let elemX2 = elemLeft + elemWidth;
        let elemY1 = elemTop;
        let elemY2 = elemTop + elemHeight;

        return {
            "positionElement":positionElement,
            "elemTop":elemTop,
            "elemLeft":elemLeft,
            "elemWidth":elemWidth,
            "elemHeight":elemHeight,
            "elemX1":elemX1,
            "elemX2":elemX2,
            "elemY1":elemY1,
            "elemY2":elemY2
        };
    }

    /**
     * проверка элемента на видимость во viewport
     * @param el - jQuery object
     * @returns {boolean}
     */
    public gtmCheckElementInViewport(el: any)
    {
        let screen = this.gtmAboutScreen();
        let element = this.gtmAboutElement(el);

        let screenWidth = screen.screenWidth;
        let screenHeight = screen.screenHeight;
        let scrollTop = screen.scrollTop;
        let scrollLeft = screen.scrollLeft;

        let seeX1 = scrollLeft;
        let seeX2 = screenWidth + scrollLeft;
        let seeY1 = scrollTop;
        let seeY2 = screenHeight + scrollTop;

        let elemTop = element.elemTop;
        let elemLeft = element.elemLeft;
        let elemWidth = element.elemWidth;
        let elemHeight = element.elemHeight;

        let elemX1 = elemLeft;
        let elemX2 = elemLeft + elemWidth;
        let elemY1 = elemTop;
        let elemY2 = elemTop + elemHeight;

        if( elemX1 >= seeX1 && elemX2 <= seeX2 && elemY1 >= seeY1 && elemY2 <= seeY2 ){
            return true;
        }

        return false;
    }

    public test = 'class GtmViewportParent';

    public toConsole(): void {
        console.log(this.test);
    }

}