/**
 * Created by p.vasin on 24.08.17.
 * работа с viewport
 */
import GtmCommon from './gtm-common';
import GtmObject from './gtm-object';
import GtmWidget from './gtm-widget';
import GtmSlider from './gtm-slider';
import GtmViewportParent from './gtm-viewport-parent';
export default class GtmViewport extends GtmViewportParent {

    /**
     * выполнение логики на document ready
     * @param GtmWidget widgetObject
     * @param GtmSlider sliderObject
     */
    public run(widgetObject: GtmWidget,sliderObject: GtmSlider): void {

        let thisClass = this; // для использования внутри let
        let widget: GtmWidget = widgetObject;
        let slider: GtmSlider = sliderObject;

        (<any>window).onload = function() {
            document.querySelector("a").focus();
        };
        (<any>window).onfocus = function() {
            this.gtmFocusedBrowser = true;
        };
        (<any>window).onblur = function() {
            this.gtmFocusedBrowser = false;
        };

        // учет баннеров из bx-слайдера
        slider.bxSliderRecord();

        // родитель для продуктов
        let gtmWrapProds = GtmCommon.getProdConf('parent');

        // для главной страницы из виджетов вибираем только видимые
        if (jsonPage.pageCategory === "Main") {
            gtmWrapProds = gtmWrapProds + ":visible";
        }

        // выборка статичных баннеров
        let bannersVisible = jQuery(".m-banner img:visible[data-banner-id], .widget-banner img:visible[data-banner-id], .action-block img:visible[data-banner-id]");

        let sliderRevolutionHome = (<any>window).sliderRevolutionHome;
        if (typeof sliderRevolutionHome !== 'undefined' && sliderRevolutionHome.length) {
            let revapi = sliderRevolutionHome.show().revolution({
                viewPort: {
                    enable: true,
                    outof: 'pause',
                    visible_area: '50%',
                    presize: true
                }
            });

            revapi.on('revolution.slide.onchange', function(event, data) {
                console.log('revolution slide onchange');
                let currentSlide = data.slide.find('.tp-bgimg');
                let bannID = currentSlide.attr('src');
                let showImgSlide = thisClass.gtmCheckElementInViewport(data.slide);
                let targetBanner = null;
                let oneSlide: any;
                for (let key in jsonBanners) {
                    if (bannID === jsonBanners[key]['src']) {
                        oneSlide = GtmObject.getPromotionObject();
                        targetBanner = jsonBanners[key];
                        oneSlide.id = targetBanner.id;
                        oneSlide.name = targetBanner.name;
                        oneSlide.creative = thisClass.gtmCreativeForSlider;
                        oneSlide.position = targetBanner.position;
                    }
                }
                // уникальный пушинг каждого просмотра если баннер во вьювпорте
                if (showImgSlide && typeof oneSlide !== 'undefined') {
                    slider.gtmPushOneBanner(oneSlide);
                }
            });
        };

        // на первичную загрузку страницы
        widget.gtmCheckProdPosition(gtmWrapProds);
        slider.gtmCheckBannerPosition(bannersVisible);

        jQuery(document).on('scroll',function(){
            slider.bxSliderRecord();
            widget.gtmCheckProdPosition(gtmWrapProds);
            slider.gtmCheckBannerPosition(bannersVisible);
        });

        jQuery(window).on('resize',function(){
            slider.bxSliderRecord();
            widget.gtmCheckProdPosition(gtmWrapProds);
            slider.gtmCheckBannerPosition(bannersVisible);
        });

        // учет товаров из виджетов только там где они есть
        if (jQuery(".widget, #upsell-product-table, .block-related").length>0) {
            let shownProds = jQuery(GtmCommon.getProdConf('shown'));
            widget.gtmIterateWidget(shownProds);
            jQuery('.nav-pills a').on('shown.bs.tab', function (e) {
                let pill = jQuery(this);
                let needTab = pill.parents('ul.nav-pills').next('.tab-content');
                let shownProdsPill = needTab.find(GtmCommon.getProdConf('pill'));
                widget.gtmIterateWidget(shownProdsPill);
            });
        }

    }

}