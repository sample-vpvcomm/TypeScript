/**
 * Created by p.vasin on 24.08.17.
 * обработка данных из виджетов
 */
import GtmCommon from './gtm-common';
import GtmObject from './gtm-object';
import GtmViewportParent from './gtm-viewport-parent';
export default class GtmWidget extends GtmViewportParent {

    /**
     * вычисление позиции товара во viewport
     * @param divParent - jQuery object
     */
    public gtmCheckProdPosition(divParent: any): void {
        let thisClass = this; // для использования внутри let
        let arr = jQuery(divParent).children();
        jQuery.each(arr, function () {
            let item = jQuery(this);
            let checkViewport = thisClass.gtmCheckElementInViewport(item);
            if (checkViewport) {
                let sku = item.find(GtmCommon.getProdConf('sku')).text().replace(/\n/g, '').trim();
                let descr = jsonProducts[sku];
                if (typeof descr !== 'undefined') {
                    thisClass.gtmImpressProducts[sku] = descr;
                    clearTimeout(thisClass.gtmPushTimerProducts);
                    thisClass.gtmPushTimerProducts = setTimeout(function(){
                        let result = [];
                        let object = thisClass.gtmImpressProducts;
                        for (let key in object) {
                            let value = object[key];
                            if (typeof value !== 'undefined') {
                                result.push(value);
                            }
                        }
                        while (result.length > 0) {
                            let forPush = GtmObject.getEventObject(result.splice(0, thisClass.chunkSize));
                            dataLayer.push(forPush);
                        }
                    }, thisClass.gtmPushTimerCycle);
                }
            }

        });
    }

    /**
     * перебор товаров из виджетов
     * @param array
     */
    public gtmIterateWidget(array: any): void {
        let result = [];
        let pageType = dataLayer[0]["pageType"];
        let middleStore = {};
        if (typeof jsonSellProducts !== 'undefined') {
            jQuery.extend(true,jsonProducts,jsonSellProducts);
        }
        jQuery.each(array,function(){
            let item = jQuery(this);
            // получение названия виджета
            let itemH2 = item.parents('.tab-content').prevUntil('.widget').prev().find('h2').text();
            let itemSku = item.find(GtmCommon.getProdConf('sku')).text().replace(/\n/g, '').trim();
            let itemDataSku = item.data('sku');
            if (typeof itemDataSku !== 'undefined') {
                itemSku = itemDataSku;
            }

            itemSku = itemSku.toString().replace(/Арт./gi,''); // удаление префикса для Rowenta
            let descr = jsonProducts[itemSku];

            if (typeof descr !== 'undefined') {
                if (jQuery.isPlainObject(descr) && descr.hasOwnProperty('list')) {
                    descr.list = (typeof itemH2 !== 'undefined' && itemH2 !== '') ? itemH2 : pageType;
                }
                // новое получение названия виджета из атрбута data-widget-name шаблона widget/grid
                let widget_name = item.parents('ul,.masonry-grid-fitrows').data('widget-name');
                if (typeof widget_name !== 'undefined') {
                    descr.list = widget_name;
                }
                middleStore[itemSku] = descr;
            }

        });
        for (let key in middleStore) {
            let value = middleStore[key];
            if (typeof value !== 'undefined') {
                result.push(value);
            }
        }
        let forPushWidget = GtmObject.getEventObject(result);
        dataLayer.push(forPushWidget);
    }

    public test = 'class GtmWidget';

    public toConsole(): void {
        console.log(this.test);
    }

}